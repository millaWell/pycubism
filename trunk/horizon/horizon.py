#!/usr/bin/env python
# encoding: utf-8

import sys
import os
import numpy as np
import matplotlib.pyplot as plt

def horizonPlot(x,y,height=50.0,colors=['CornflowerBlue','DarkGreen']):
	def layer(y,height):
		neg=0.0;pos=0.0
		if y>0:
			if y-height>=0:
				pos=height
				y-= pos
			else : 
				pos = y
		elif y<0:
			if y+height<=0:
				neg=height
				y += neg
			else : 
				neg = -y
		return pos,neg
	
	alpha = .20
	vlayer = np.vectorize(layer)
	while (y != 0).any():
		l = vlayer(y,height)
		y -= l[0];y += l[1]
		plt.fill_between(x,0,l[0],color=colors[0], alpha=alpha)
		plt.fill_between(x,height-l[1],height,color=colors[1], alpha=alpha)