import matplotlib.pyplot as plt
from horizon import horizonPlot
import numpy as np
from copy import deepcopy

def ex1():
	x = np.linspace(0, np.pi*4, 137)
	y = (2*np.random.normal(size=137) + x**2)
	xx = np.hstack([-1*x[::-1], x])
	yy = np.hstack([-1*y[::-1], y])
	f = plt.figure()
	horizonPlot(xx,yy)
	plt.show()
	


def ex2():
	nsubplot = 4
	x = np.linspace(0, np.pi*4, 137)
	y = (2*np.random.normal(size=137) + x**2)
	xx = np.hstack([-1*x[::-1], x])
	yy = np.hstack([-1*y[::-1], y])
	height = 10.0
	f = plt.figure()
	subplots = list()
	
	for fi in np.arange(0,nsubplot):
		sf = f.add_subplot(nsubplot,1,fi)
		horizonPlot(deepcopy(xx),deepcopy(yy),height)
		sf.axis([min(xx),max(xx),0,height])
		if fi != 0: sf.set_xticklabels([])
		sf.grid(True)
		sf.set_yticks([0,10])
		subplots.append(sf)
		
	plt.show()

